# PhotosFS

A FUSE filesystem for presenting a photo library from Apple's macOS Photos as a
reasonably browseable read-only filesystem.

Photos/videos are identified by their creation dates, so having files that
share a date will result in warnings and one of the files being inaccessible.

By default edited versions of photos are shown, adding ".orig" after the date
will show the original.
