# (c) 2022 David Koňařík
# Licenced under GNU GPL v3 or later
import datetime as dt
import sqlite3
import threading
import trio
from typing import List
from collections import defaultdict
from dataclasses import dataclass


@dataclass
class Album:
    id: int
    name: str
    parent: object
    start_date: dt.datetime


@dataclass
class HierAlbum:
    id: int
    name: str
    children: List["HierAlbum"]
    start_date: dt.datetime


@dataclass
class Photo:
    id: int
    filepath: str
    created: dt.datetime


EPOCH_DIFF = (dt.datetime(2001, 1, 1, 0, 0)
              - dt.datetime(1970, 1, 1, 0, 0)).total_seconds()


def dt_from_timestamp(timestamp):
    return dt.datetime.fromtimestamp((timestamp or 0) + EPOCH_DIFF)


class PhotosDb:
    class ThreadLocalData(threading.local):
        db_conn: sqlite3.Connection

    path: str
    thread_local = ThreadLocalData()

    def __init__(self, path):
        self.path = path
        self.thread_local = threading.local()

    def query_inner(self, sql, params=[]):
        if not hasattr(self.thread_local, "db_conn"):
            self.thread_local.db_conn = sqlite3.connect(
                    f"file:{self.path}?mode=ro", uri=True)
        return list(self.thread_local.db_conn.cursor().execute(sql, params))

    async def query(self, sql, params=[]):
        return await trio.to_thread.run_sync(self.query_inner, sql, params)

    async def albums(self):
        for r in await self.query("""
                SELECT Z_PK, ZTITLE, ZPARENTFOLDER, ZSTARTDATE
                FROM ZGENERICALBUM
                WHERE ZKIND IN (2, 3999, 4000)"""):
            yield Album(r[0], r[1], r[2], dt_from_timestamp(r[3]))

    async def album_hierarchy(self):
        root = None
        by_parent = defaultdict(list)
        async for a in self.albums():
            if a.parent is None:
                root = HierAlbum(a.id, a.name, [], a.start_date)
            else:
                by_parent[a.parent].append(
                        HierAlbum(a.id, a.name, [], a.start_date))
        q = [root]
        while len(q) > 0:
            a = q.pop()
            a.children = by_parent[a.id]
            q += a.children
        return root

    async def photos_in_album(self, album_id):
        for r in await self.query("""
                SELECT
                    A.Z_PK,
                    A.ZDIRECTORY || '/' || A.ZFILENAME,
                    A.ZDATECREATED
                FROM Z_26ASSETS AS M
                LEFT JOIN ZASSET AS A ON M.Z_3ASSETS = A.Z_PK
                WHERE M.Z_26ALBUMS IS ?
                """, [album_id]):
            yield Photo(r[0], r[1], dt_from_timestamp(r[2]))

    async def photo_by_album_and_created(self, album_id, created):
        rows = await self.query("""
                SELECT
                    A.Z_PK,
                    A.ZDIRECTORY || '/' || A.ZFILENAME,
                    A.ZDATECREATED
                FROM Z_26ASSETS AS M
                LEFT JOIN ZASSET AS A ON M.Z_3ASSETS = A.Z_PK
                WHERE M.Z_26ALBUMS IS ?
                  AND A.ZDATECREATED = ?
                """, [album_id,
                      dt.datetime.timestamp(created) - EPOCH_DIFF])
        r = next(iter(rows), None)
        if r: return Photo(r[0], r[1], dt_from_timestamp(r[2]))

    async def photo_by_id(self, photo_id):
        rows = await self.query("""
                SELECT
                    A.Z_PK,
                    A.ZDIRECTORY || '/' || A.ZFILENAME,
                    A.ZDATECREATED
                FROM ZASSET AS A
                WHERE A.Z_PK = ?
                """, [photo_id])
        r = next(iter(rows), None)
        if r: return Photo(r[0], r[1], dt_from_timestamp(r[2]))

    async def get_edited_filename(self, photo):
        uuid = photo.filepath.split("/")[-1].split(".")[0]
        return f"resources/renders/{uuid[0]}/{uuid}_1_201_a.jpeg"
