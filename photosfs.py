#!/usr/bin/env python3
# (c) 2022 David Koňařík
# Licenced under GNU GPL v3 or later
import os
import pathlib
import datetime as dt
import errno
import stat
import sys
import trio
import pyfuse3
from enum import IntEnum
from typing import List, Dict, Tuple
from collections import Counter
from dataclasses import dataclass
from photosdb import PhotosDb, HierAlbum


# Inode scheme:
#   2 LSB indicate the kind
#   The rest of the inode is the database ID
#   Special album with ID 2**32-1 has photos without any album
class InodeKind(IntEnum):
    ORIGINAL = 1
    ALBUM = 2
    EDITED = 3


@dataclass
class OpenedDirData:
    context: pyfuse3.RequestContext
    last_idx: int
    entries: List[Tuple[int, str]]


class PhotosFs(pyfuse3.Operations):
    library_path: pathlib.Path
    photos_db: PhotosDb
    root_album: HierAlbum
    album_by_inode: Dict[int, HierAlbum]
    last_opened_dir_handle: int
    opened_dirs: Dict[int, OpenedDirData]

    ALBUMLESS_ALBUM_ID = 2**32-1

    # Photos are named as <creation time>.<extension>
    photo_date_format = "%Y-%m-%d %H:%M:%S:%f"

    def __init__(self, library_path, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.library_path = library_path
        db_path = self.library_path / "database" / "Photos.sqlite"
        self.photos_db = PhotosDb(db_path)
        self.last_opened_dir_handle = 0
        self.opened_dirs = {}

    # Custom methods have leading underscores to distinguish them from FUSE
    # handlers

    def _construct_inode(self, kind, idnum):
        return kind | (idnum << 2)

    def _deconstruct_inode(self, inode):
        # Returns (kind, idnum)
        return (inode & 0b11, inode >> 2)

    def _safe_filename(self, orig_name):
        # Album names can have slashes, paths can't
        # Maybe I'll someday expand this to include Windows' or macOS'
        # forbidden characters
        return orig_name.replace("/", "\u2044")

    async def _load_data(self):
        self.root_album = await self.photos_db.album_hierarchy()
        self.album_by_inode = {pyfuse3.ROOT_INODE: self.root_album}

        def add_album(a):
            inode = self._construct_inode(InodeKind.ALBUM, a.id)
            self.album_by_inode[inode] = a
            for c in a.children:
                add_album(c)
        add_album(self.root_album)

        # Add "Albumless Photos" special album
        aa = HierAlbum(None, "_Albumless Photos", [], dt.datetime.min)
        aa_inode = self._construct_inode(InodeKind.ALBUM, self.ALBUMLESS_ALBUM_ID)
        self.album_by_inode[aa_inode] = aa
        self.root_album.children.append(aa)

    async def _get_inode_backing_path(self, inode):
        kind, idnum = self._deconstruct_inode(inode)
        photo = await self.photos_db.photo_by_id(idnum)
        if kind == InodeKind.EDITED:
            local_path = await self.photos_db.get_edited_filename(photo)
            path = self.library_path / local_path
            # Default to edited (without any extension), show the original
            # if that doesn't exist
            if path.exists():
                return path

        return self.library_path / "originals" / photo.filepath

    async def statfs(self, ctx):
        stat = pyfuse3.StatvfsData()
        return stat

    async def lookup(self, parent_inode, name, ctx):
        if parent_inode not in self.album_by_inode:
            raise pyfuse3.FUSEError(errno.ENOENT)
        parent = self.album_by_inode[parent_inode]

        if name == ".":
            return await self._getattr(parent_inode, ctx)
        if name == "..":
            # TODO: Actually find parent
            return await self._getattr(parent_inode, ctx)

        inode = None

        album = next((
                a for a in parent.children
                # We can't compare the strings directly, because somewhere
                # along the way, Unicode normalisation one way or the other
                # happens
                if os.fsencode(a.name) == os.fsencode(name)
            ), None)
        if album:
            inode = self._construct_inode(
                InodeKind.ALBUM,
                album.id or self.ALBUMLESS_ALBUM_ID)
        else:
            name_split = os.fsdecode(name).split(".")
            try:
                photo_dt = dt.datetime.strptime(
                    name_split[0], self.photo_date_format)
            except ValueError:
                raise pyfuse3.FUSEError(errno.ENOENT)

            photo = await self.photos_db.photo_by_album_and_created(
                parent.id, photo_dt)
            if photo is not None:
                ext = photo.filepath.split(".")[-1]
                if ext != name_split[-1]:
                    raise pyfuse3.FUSEError(errno.ENOENT)

                if len(name_split) > 1 and name_split[1] == "orig":
                    kind = InodeKind.ORIGINAL
                else:
                    kind = InodeKind.EDITED

                inode = self._construct_inode(kind, photo.id)

        if inode is None:
            raise pyfuse3.FUSEError(errno.ENOENT)
        else:
            return await self.getattr(inode)

    async def opendir(self, inode, ctx):
        if inode not in self.album_by_inode:
            raise pyfuse3.FUSEError(errno.ENOTDIR)

        album = self.album_by_inode[inode]
        entries = []
        for c in album.children:
            id = c.id or self.ALBUMLESS_ALBUM_ID
            entry_inode = self._construct_inode(InodeKind.ALBUM, id)
            entries.append((entry_inode, c.name))
        async for p in self.photos_db.photos_in_album(album.id):
            entry_inode = self._construct_inode(InodeKind.EDITED, p.id)
            ext = p.filepath.split(".")[-1]
            name = p.created.strftime(self.photo_date_format) + "." + ext
            entries.append((entry_inode, name))
        entries.sort(key=lambda e: e[0])

        for filename, count in Counter(e[1] for e in entries).items():
            if count > 1:
                print(f"Duplicate filenames in album '{album.name}'! '{filename}'")

        handle = self.last_opened_dir_handle
        self.last_opened_dir_handle += 1

        self.opened_dirs[handle] = \
            OpenedDirData(
                context=ctx,
                last_idx=0,
                entries=entries)

        return handle

    async def releasedir(self, handle):
        if handle not in self.opened_dirs:
            raise pyfuse3.FUSEError(errno.ENOENT)
        del self.opened_dirs[handle]

    async def readdir(self, handle, start_id, token):
        # start_id is 0 or last inode given
        if handle not in self.opened_dirs:
            raise pyfuse3.FUSEError(errno.ENOENT)

        data = self.opened_dirs[handle]

        if len(data.entries) > data.last_idx \
                and data.entries[data.last_idx][0] <= start_id:
            iter_start_idx = data.last_idx
        else:
            iter_start_idx = 0
        entries = data.entries[iter_start_idx:]

        i = 0
        for i, (child_inode, name) in enumerate(entries):
            if child_inode <= start_id:
                continue
            if not pyfuse3.readdir_reply(
                    token,
                    os.fsencode(self._safe_filename(name)),
                    await self.getattr(child_inode, data.context),
                    child_inode):
                break

        data.last_idx = iter_start_idx + i

    async def getattr(self, inode, ctx=None):
        entry = pyfuse3.EntryAttributes()
        kind, idnum = self._deconstruct_inode(inode)
        if kind == InodeKind.ALBUM or inode == pyfuse3.ROOT_INODE:
            if inode not in self.album_by_inode:
                raise pyfuse3.FUSEError(errno.ENOTDIR)
            album = self.album_by_inode[inode]

            entry.st_mode = (stat.S_IFDIR | 0o555)
            entry.st_size = 0

            try:
                stamp = int(dt.datetime.timestamp(album.start_date)
                            * 1000000000)
            except ValueError:
                stamp = 0
            entry.st_atime_ns = stamp
            entry.st_ctime_ns = stamp
            entry.st_mtime_ns = stamp
        elif kind in [InodeKind.ORIGINAL, InodeKind.EDITED]:
            entry.st_mode = (stat.S_IFREG | 0o444)

            path = await self._get_inode_backing_path(inode)
            try:
                photo_stat = path.stat()
            except FileNotFoundError:
                raise pyfuse3.FUSEError(errno.ENOENT)
            entry.st_size = photo_stat.st_size
            entry.st_atime_ns = photo_stat.st_atime_ns
            entry.st_ctime_ns = photo_stat.st_ctime_ns
            entry.st_mtime_ns = photo_stat.st_mtime_ns
        else:
            raise pyfuse3.FUSEError(errno.ENOENT)

        entry.st_gid = ctx.gid if ctx is not None else os.getgid()
        entry.st_uid = ctx.uid if ctx is not None else os.getuid()
        entry.st_ino = inode

        return entry

    async def open(self, inode, flags, ctx):
        kind, idnum = self._deconstruct_inode(inode)
        if kind not in [InodeKind.ORIGINAL, InodeKind.EDITED]:
            raise pyfuse3.FUSEError(errno.ENOENT)

        # TODO: Why is this needed?
        flags &= 2**15 - 1

        if flags & ~(os.O_RDONLY | os.O_DIRECT | os.O_NONBLOCK | os.O_NOATIME):
            raise pyfuse3.FUSEError(errno.EACCES)

        path = await self._get_inode_backing_path(inode)
        try:
            backing_fd = os.open(path, flags)
        except OSError as exc:
            raise pyfuse3.FUSEError(exc.errno)
        return pyfuse3.FileInfo(fh=backing_fd)

    async def read(self, fh, off, size):
        try:
            os.lseek(fh, off, os.SEEK_SET)
            return os.read(fh, size)
        except OSError as exc:
            raise pyfuse3.FUSEError(exc.errno)

    async def release(self, fh):
        try:
            os.close(fh)
        except OSError as exc:
            raise pyfuse3.FUSEError(exc.errno)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print(f"Usage: {sys.argv[0]} <fuse options> <.photoslibrary> <mountpoint>",
              file=sys.stderr)
        sys.exit(1)

    fs = PhotosFs(pathlib.Path(sys.argv[-2]))
    trio.run(fs._load_data)
    fuse_options = set(pyfuse3.default_options)
    fuse_options.add("fsname=photosfs")
    for opt in sys.argv[1:-2]:
        fuse_options.add(opt)
    pyfuse3.init(fs, sys.argv[-1], fuse_options)
    try:
        trio.run(pyfuse3.main)
    finally:
        pyfuse3.close()
